package com.endava.moviecatalogservice.controller;

import com.endava.moviecatalogservice.model.CatalogItem;
import com.endava.moviecatalogservice.model.Movie;
import com.endava.moviecatalogservice.model.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResource {

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("/{userId}")
    public List<CatalogItem> getCatalog(@PathVariable("userId") String userId) {
        //get all Rated movie Ids for this user
        UserRating userRating = restTemplate.getForObject("http://ratings-service/ratingsdata/users/" + userId, UserRating.class);

        return userRating.getRatingList().stream()
                .map(rating -> {
                    //for each movie Id, call movie info service and get the details
                    Movie movie = restTemplate.getForObject("http://movie-info-service/movies/" + rating.getMovieId(), Movie.class);
                    //put them all together
                    return new CatalogItem(movie.getName(), "Desc", rating.getRating());
                })
                .collect(Collectors.toList());
    }

}
